export default class Card {

    /**
     * On créé la card
     */
    card() {
        document.getElementById("timer_button").addEventListener('click', () => {
            let infos = [{ name: "planets", max: 60 }, { name: "species", max: 37 },
            { name: "starships", max: 36 }, { name: "vehicles", max: 39 }, { name: "people", max: 82 }]

            // Catégorie random, on fait un Math.floor() pour arrondir à l'unité
            let randomIndex = Math.floor(Math.random() * infos.length);
            const category = infos[randomIndex];

            // ID aléatoire en fonction de la catégories
            let randomMax = Math.floor(Math.random() * category.max);
            const url = `https://swapi.dev/api/${category.name}/${randomMax}`
            fetch(String(url)).then((data) => {
                return data.json()
            }).then((datas) => {

                if (datas.detail === "Not found") {
                    this.card()
                } else {
                    
                    console.log(datas);
                    let card_div = document.getElementById("card_result")
                    let card_title = document.getElementById("card_title")
                    card_div.className = "card mx-auto mt-1 text-center"
                    card_title.innerHTML = `${datas.name}`
                    console.log(datas.name);
                    if(localStorage.getItem(`datas`) === datas){
                        console.log("déjà présent");
                    } else {
                        this.saveCard(datas)
                    }
                    this.showAllCards()
                }
            }).catch((error) => {
                console.log(error);
                this.card()
            })
        })
    }

    /**
     * Sauvegarde la card dans le localStorage
     * @param datas Data reçue
     */
    saveCard(datas: any){
        const allData = JSON.parse(localStorage.getItem('datas'));
        if (allData){
            allData.push(datas);
            localStorage.setItem('datas', JSON.stringify(allData));
        }else{
            localStorage.setItem('datas', JSON.stringify([datas]));
        }
    }

    /**
     * Afficher toutes les cartes
     */
    showAllCards(){
        const card_list_section = document.getElementById("cards_list_section")
        card_list_section.className = "d-flex flex-column"
        let data = localStorage.getItem(`datas`)
        let json_parse = JSON.parse(data)
        const allData = JSON.parse(localStorage.getItem('datas'));
        console.log("mes data", data);
        console.log("parsing json",json_parse);
        allData.map((x: any) => {
            const div_card = document.createElement("div")
            div_card.id = `id`
            div_card.style.width = "18rem"
            div_card.className = "card mx-auto mt-1 text-center"
            const div = document.createElement("div")
            div.className = ""
            div.innerHTML += `
            <h2>${x.name}</h2>
            `
            card_list_section.appendChild(div_card)
            div_card.appendChild(div)
            document.body.appendChild(card_list_section)
        })
        
    }
}