const anime = require('animejs');

export default class Timer {

    /**
     * Méthode du Timer
     */
    timer() {
        let button = document.getElementById("timer_button");
        let countdown_div = document.getElementById("countdown")
        let hours_section = document.getElementById("hours")
        let minutes_section = document.getElementById("minutes")
        let seconds_section = document.getElementById("seconds")
        button.addEventListener('click', () => {
            countdown_div.className = ""
            let newTime = new Date().getTime()

            if (localStorage.getItem("temp") !== null) {

                let timeUser = parseInt(localStorage.getItem("temp")) - newTime

                if (timeUser <= 0) {
                    localStorage.removeItem("temp")

                    hours_section.innerHTML = `0`
                    minutes_section.innerHTML = `0`
                    seconds_section.innerHTML = `0`
                    button.className = "btn btn-primary btn-lg"
                    clearInterval()
                }
            } else {
                // On calcule les ms actuelles pour ajouter 2h
                localStorage.setItem("temp", String(new Date().getTime() + 3600 * 2 * 1000))
            }
            let timeUser = parseInt(localStorage.getItem("temp")) - newTime

            const x = setInterval(() => {
                let newTimeTwo = new Date().getTime()
                let timeUserTwo = parseInt(localStorage.getItem("temp"))
                let tempTime = timeUserTwo

                localStorage.setItem("temp", String(tempTime))

                let timeDCR = parseInt(localStorage.getItem("temp")) - newTimeTwo
                let secondesTime = timeDCR / 1000
                let result = this.showTimes(secondesTime).split(":")

                hours_section.innerHTML = result[0] + "h"
                minutes_section.innerHTML = result[1] + "m"
                seconds_section.innerHTML = result[2] + "s"
                button.className = "btn btn-primary btn-lg disabled"

                if (timeUser <= 0) {
                    button.className = "btn btn-primary btn-lg"
                    clearInterval(x)
                }
            }, 1000)
        });
    }

    /**
     * Affiche les valeurs du temp restant
     * @param value Nombre de secondes
     * @return heures, minutes et secondes
     */
    showTimes(value: number) {
        let sec_num = value

        let hours = Math.floor(sec_num / 3600);
        let minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        let seconds = Math.round(sec_num - (hours * 3600) - (minutes * 60));

        return hours + ':' + minutes + ':' + seconds;
    }
}
